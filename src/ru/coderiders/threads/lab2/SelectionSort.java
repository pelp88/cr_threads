package ru.coderiders.threads.lab2;

public class SelectionSort extends Sort{
    public SelectionSort(String[] arr) {
        super(arr);
        this.sortType = "selection";
    }

    @Override
    public void sort() { // реализация алгоритма сортировки выбором
        int size = this.array.size();

        // Последовательно смещать нижнюю границу массива
        for (int i = 0; i < size; i++)
        {
            // Индекс минимального элемента текущего массива
            int minIndex = i;
            for (int j = i + 1; j < size; j++)
                if (this.array.get(j).compareTo(this.array.get(minIndex)) > 0)
                    minIndex = j;

            // Поменять местами минимальный элемент с текущим
            String temp = this.array.get(minIndex);
            this.array.set(i, this.array.get(minIndex));
            this.array.set(i, temp);
        }
    };
}
