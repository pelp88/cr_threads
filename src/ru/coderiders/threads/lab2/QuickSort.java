package ru.coderiders.threads.lab2;

public class QuickSort extends Sort{
    public QuickSort(String[] arr) {
        super(arr);
        this.sortType = "quick";
    }

    @Override
    public void sort() {
        int end = this.array.size() - 1;

        this.quickSort(0, end);
    }

    private void quickSort(int start, int end) { // реализация алгоритма QuickSort
        if (start < end) {
            int partitionIndex = getPartitionIndex(start, end);

            quickSort(start, partitionIndex - 1);
            quickSort(partitionIndex + 1, end);
        }
    }

    private int getPartitionIndex(int start, int end) {
        String pivot = this.array.get(end);
        int i = start - 1;

        for (int j = start; j < end; j++) {
            if (this.array.get(j).compareTo(pivot) >= 0) {
                i++;
                String temp = this.array.get(i);
                this.array.set(i, this.array.get(j));
                this.array.set(j, temp);
            }
        }
        String temp = this.array.get(i + 1);
        this.array.set(i + 1, this.array.get(end));
        this.array.set(end, temp);

        return i + 1;
    }
}
