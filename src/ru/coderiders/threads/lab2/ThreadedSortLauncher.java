package ru.coderiders.threads.lab2;

public class ThreadedSortLauncher {
    public static void main(String[] args) {
        String[] test = {"One", "десять", "1000", "Sieben", "hachi"};


        Thread insertion = new InsertionSort(test.clone());
        Thread quick = new QuickSort(test.clone());
        Thread selection = new SelectionSort(test.clone()); //сортировка немного сломана, не успел поправить

        insertion.start();
        quick.start();
        selection.start();
    }
}
