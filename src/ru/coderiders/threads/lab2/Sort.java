package ru.coderiders.threads.lab2;

import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

public abstract class Sort extends Thread{
    public Vector<String> array;
    public String sortType;

    Sort(String[] arr) {
        this.array = new Vector<String>(Arrays.asList(arr));
    }

    @Override
    public void run(){
        sort();
        System.out.println(this.getName() +
                "   " + sortType + "   " + Arrays.toString(this.result()));
    }

    abstract void sort();

    public String[] result() {
        return this.array.toArray(new String[this.array.size()]);
    }

    public String[] resultDescending() {
        Vector<String> temp = this.array;
        Collections.reverse(temp);
        return temp.toArray(new String[temp.size()]);
    }
}
