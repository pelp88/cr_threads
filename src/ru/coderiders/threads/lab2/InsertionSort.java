package ru.coderiders.threads.lab2;

public class InsertionSort extends Sort{
    public InsertionSort(String[] arr) {
        super(arr);
        this.sortType = "insertion";
    }

    @Override
    public void sort() { // реализация алгоритма сортировки вставками
        int end = this.array.size();

        // итерация через массив
        for (int i = 1; i < end; ++i) {
            String elem = this.array.get(i);

            // переместить все элементы подмассива больше чем elem, на одну позицию вперед
            int j = i - 1;
            while (j >= 0 && this.array.get(j).compareTo(elem) < 0) {
                this.array.set(j + 1, this.array.get(j));
                j = j - 1;
            }
            this.array.set(j + 1, elem);
        }
    }
}
