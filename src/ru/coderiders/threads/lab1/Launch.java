package ru.coderiders.threads.lab1;

import java.util.concurrent.atomic.AtomicBoolean;

public class Launch {
    public static AtomicBoolean flag = new AtomicBoolean(false);

    public static void main(String[] args) {
        Thread egg = new Thread(() -> {
            for (int i = 0; i <= 5; i++) {
                flag.set(true);
                try {
                    System.out.println("яйцо");
                    Thread.sleep((int) (Math.random() * 1000));
                } catch (InterruptedException e) {
                    break;
                }
            }
        });

        Thread chicken = new Thread(() -> {
            for (int i = 0; i <= 5; i++) {
                flag.set(false);
                try {
                    System.out.println("курица");
                    Thread.sleep((int) (Math.random() * 1000));
                } catch (InterruptedException e) {
                    break;
                }
            }
        });

        egg.start();
        chicken.start();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ignored) {}

        System.out.println("победитель: " + (flag.get() ? "яйцо" : "курица"));
        egg.interrupt();
        chicken.interrupt();
    }
}
